<?php

namespace App\Services\Idea\User;

use App\Services\Idea\User\Handlers\TagJoinHandler;

class UserService
{
    protected TagJoinHandler $joinHandler;

    public function __construct(TagJoinHandler $joinHandler)
    {
        $this->joinHandler = $joinHandler;
    }

    public function joinToTag(int $tagId)
    {
        return $this->joinHandler->handle($tagId);
    }
}

