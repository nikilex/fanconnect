<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'max:255|nullable',
            'email'     => 'email|nullable',
            'phone'     => 'max:255|nullable',
            'club_id'   => 'exists:App\Models\Club,id|nullable',
            'club_name' => 'max:255|nullable',
        ];
    }
}
