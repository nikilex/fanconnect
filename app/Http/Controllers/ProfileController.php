<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ProfileRequest;
use App\Models\Club;

class ProfileController extends Controller
{
    public function index()
    {
        return Profile::all();
    }

    public function update(ProfileRequest $request)
    {
        $profile = Profile::where('user_id', Auth::id());
        $club = '';
       
        $clubExist = Club::where('name',$request->club_name);
        if ($request->club_name)
        {
            if ($clubExist->get()->isEmpty())
            {
                $club = Club::create([
                    'name' => $request->club_name,
                ]);
            } else
            {
                $club = $clubExist->first();
            }
        }
        if ($profile->get()->isEmpty())
        {
           $profile->create([
                'name'    => $request->name,
                'email'   => $request->email,
                'phone'   => $request->phone,
                'user_id' => Auth::id(),
                'club_id' => $club->id,
            ]);
        } else 
        {
            $profile->update([
                'name'    => $request->name,
                'email'   => $request->email,
                'phone'   => $request->phone,
                'user_id' => Auth::id(),
                'club_id' => $club->id
            ]);
        }

        return redirect('/cabinet');
    }
}
