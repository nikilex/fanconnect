<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Models\Profile;
use App\Mail\Invite;
use App\Models\Club;
use App\Services\Idea\IdeaService;

class CabinetController extends Controller
{
    /**
     * @var IdeaService
     */
    protected IdeaService $ideaService;

    public function __construct(IdeaService $ideaService)
    {
        $this->ideaService = $ideaService;
    }

    public function index()
    {
        $ideas = $this->ideaService->getPaginated()->where('user_id', Auth::id());
        $user = User::with('profile')->find(Auth::id());
        $profile = Profile::where('user_id', Auth::id());
        $club = '';
        if(!$profile->get()->isEmpty()){
            $club = Club::find($user->profile->club_id);
        }

        return view('cabinet', compact('ideas', 'user', 'club'));
    }

    public function invite(Request $request)
    {
        $this->validate($request, [
            'emailFriend'                => ['required', 'string', 'max:128', 'email'],
            'g-recaptcha-response' => 'required | recaptcha',
        ]);

       Mail::to($request->emailFriend)->send(new Invite());

       return redirect('/cabinet')->with('success','Приглашение отправлено');
    }
}
