<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Idea;
use App\User;
use App\Services\Idea\IdeaService;

class AdminController extends Controller
{
    /**
     * @var IdeaService
     */
    protected IdeaService $ideaService;

    public function __construct(IdeaService $ideaService)
    {
        $this->ideaService = $ideaService;
    }

    public function index()
    {
        return view('admin.index');
    }

    public function ideas()
    {
        $ideas = $this->ideaService->getPaginated();
        return view('admin.ideas', compact('ideas'));
    }

    public function users()
    {
        $users = User::with('profile')->paginate(10);
        return view('admin.users', compact('users'));
    }

    public function published(Request $request)
    {
        $idea = Idea::where('id', $request->idea_id);
        $mark = 0;
        
        if ($idea->first()->published == 0)
        {
            $mark = 1;
        } else
        {
            $mark = 0;
        }

        $idea->update([
            'published' => $mark
        ]);

        return redirect('/idea');
    }
}
