<?php

namespace App\Http\Controllers\Idea;

use App\Http\Controllers\Controller;
use App\Services\Idea\IdeaService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class IdeaController extends Controller
{

    /**
     * @var IdeaService
     */
    protected IdeaService $ideaService;

    public function __construct(IdeaService $ideaService)
    {
        $this->ideaService = $ideaService;
    }

    public function index()
    {     
        $ideas = $this->ideaService->getPaginated();
        return view('welcome', compact('ideas')); 
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'                => ['required', 'string', 'max:128'],
            'text'                 => ['required', 'string'],
            'image'                => ['image', 'max:2048'],
            'g-recaptcha-response' => 'required | recaptcha',
        ]);

        $idea = $this->ideaService->create($request->all());
        return redirect('/idea');
    }

    public function single($id)
    {
        $ideas = $this->ideaService->find($id);
        return view('single', compact('ideas'));
    }

    public function like(Request $request)
    {
        return $this->ideaService->like($request->id);
    }

    public function dislike(Request $request)
    {
        return $this->ideaService->dislike($request->id);
    }

    public function cancel(Request $request)
    {
        return $this->ideaService->cancel($request->id);
    }
}
