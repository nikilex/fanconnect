<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Idea\IdeaService;

class SingleController extends Controller
{
    /**
     * @var IdeaService
     */
    protected IdeaService $ideaService;

    public function __construct(IdeaService $ideaService)
    {
        $this->ideaService = $ideaService;
    }

    public function index($id)
    {
        $ideas = $this->ideaService->find($id);
        return view('single', compact('ideas'));
    }
}
