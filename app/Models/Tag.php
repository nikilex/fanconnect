<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Tag extends Model
{
    protected $guarded = [];

    public function setNameAttribute($value) {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] =
            Str::slug( mb_substr($this->name, 0, 40) . "-" . Carbon::now()->format('dmyHi'), '-');
    }
}
