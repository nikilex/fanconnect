<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Cog\Contracts\Love\Reactable\Models\Reactable as ReactableContract;
use Cog\Laravel\Love\Reactable\Models\Traits\Reactable;

class Idea extends Model implements ReactableContract
{
    use Reactable;

    protected $guarded = [];
}
