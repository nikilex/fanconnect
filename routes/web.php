<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', 'IdeaController@index');

Route::get('cabinet', 'CabinetController@index')->name('cabinet')->middleware('auth');

Route::get('/create', 'IdeaController@index')->middleware('auth');
Route::get('/', 'CabinetController@index')->middleware('auth');
Route::post('/cabinet/invite', 'CabinetController@invite')->middleware('auth')->name('cabinet.invite');
Route::get('/single/{id}', 'SingleController@index');

Route::get('moderator', function () {
    return view('moderator');
});

Route::post('pro', 'IdeaController@index')->name('pro');
Route::post('/profile/update', 'ProfileController@update')->middleware('auth')->name('profile.update');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/ticket', 'TicketController@index')->name('ticket');
Route::get('/store', 'StoreController@index')->name('store');
Route::get('/admin', 'AdminController@index')->middleware('admin')->name('admin.index');
Route::get('/admin/ideas', 'AdminController@ideas')->middleware('admin')->name('admin.ideas');
Route::get('/admin/users', 'AdminController@users')->middleware('admin')->name('admin.users');
Route::post('/idea/published', 'AdminController@published')->middleware('admin')->middleware('auth');



Route::namespace('Idea')->group(function () {
    Route::get('/tags', 'TagController@index')->middleware('auth');
    Route::get('/tags/create', 'TagController@store')->middleware('auth');
    Route::get('/tags/update', 'TagController@update')->middleware('auth');
    Route::get('/tags/delete', 'TagController@delete')->middleware('auth');
    Route::get('/tags/search', 'TagController@search')->middleware('auth');
    Route::get('/tags/join', 'UserController@joinToTag')->middleware('auth');
    Route::get('/tags/{tagId}', 'TagController@find')->middleware('auth');

    Route::get('/idea/', 'IdeaController@index')->middleware('auth');
    Route::get('/idea/create', 'IdeaController@store')->middleware('auth');
    Route::post('/idea/like', 'IdeaController@like')->middleware('auth');
    Route::post('/idea/dislike', 'IdeaController@dislike')->middleware('auth');
    Route::get('/idea/cancel', 'IdeaController@cancel')->middleware('auth');

    Route::post('/idea/create', 'IdeaController@store')->middleware('auth');
});
