@extends('layouts.app')

@section('content')
<div class="container mt-md-5">
    <div class="row justify-content-center ">
        <div class="col-md-5 d-md-block d-none align-self-center">
            <img src="{{ asset('src/img/phone.png') }}" alt="">
        </div>
        <div class="col col-md-4 rounded">

            <div class="card">

                <div class="row align-items-center">
                    <div class="col pt-5 pl-5 pr-5 pb-2 text-center">
                        <img class="img-fluid" src="{{ asset('src/img/logo.png') }}" style="width: 200px;" alt="">
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="card-body">
                            <p class="text-center text-black-50">Зарегистрируйтесь, чтобы участвовать в мероприятиях болельщиков.</p>
                            <form method="POST" action="{{ route('register') }}">
                                @csrf

                                <div class="form-group row">

                                    <div class="col">
                                        <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Никнейм / Электронный адрес" value="{{ old('email') }}" required autocomplete="email">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">

                                    <div class="col">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Пароль" required autocomplete="current-password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col">
                                        <input id="password-confirm" type="password" class="form-control" placeholder="Подтвердите пароль" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>

                                <!-- <div class="form-group row">
                                    <div class="col ">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember">
                                                {{ __('Запомнить меня') }}
                                            </label>
                                        </div>
                                    </div>
                                </div> -->

                                <div class="form-group row mb-0">
                                    <div class="col ">
                                        <button type="submit" class="btn btn-outline-primary btn-block">
                                            {{ __('Регистрация') }}
                                        </button>
                                    </div>
                                </div>
                                <div class="form-group row mt-3 mb-0">
                                    <div class="col text-center text-black-50">
                                        <p>Регистрируясь, вы принимаете наши Условия, Политику использования данных и Политику в отношении файлов cookie.</p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
            <div class="card mt-3">
                <div class="row p-3">
                    <div class="col text-center">
                        <p>Есть аккаунт? <a href="/login">Войти</a></p>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col text-center">
                    <p>Скоро на мобильных устройствах</p>
                </div>
            </div>
            <diw class="row">
                <div class="col text-center">
                    <img src="{{ asset('src/img/apple.png') }}" style="height: 40px;" alt="">
                </div>
                <div class="col text-center">
                    <img src="{{ asset('src/img/android.png') }}" style="height: 40px;" alt="">
                </div>
            </diw>
        </div>
    </div>
</div>
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Регистрация') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Имя') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail адрес') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Пароль') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Подтвердите пароль') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Регистрация') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection