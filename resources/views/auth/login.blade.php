@extends('layouts.app')

@section('content')
<div class="container mt-md-5">
    <div class="row justify-content-center ">
        <div class="col-md-5 d-md-block d-none">
            <img src="{{ asset('src/img/phone.png') }}" alt="">
        </div>
        <div class="col col-md-4 rounded">

            <div class="card">

                <div class="row align-items-center">
                    <div class="col p-5 text-center align-self-center">
                        <img class="img-fluid" src="{{ asset('src/img/logo.png') }}" style="width: 200px;" alt="">
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="card-body">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf

                                <div class="form-group row">

                                    <div class="col">
                                        <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Электронный адрес" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">

                                    <div class="col">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Пароль" required autocomplete="current-password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <!-- <div class="form-group row">
                                    <div class="col ">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember">
                                                {{ __('Запомнить меня') }}
                                            </label>
                                        </div>
                                    </div>
                                </div> -->

                                <div class="form-group row mb-0">
                                    <div class="col ">
                                        <button type="submit" class="btn btn-outline-primary btn-block">
                                            {{ __('Логин') }}
                                        </button>
                                    </div>
                                </div>
                                <!-- <div class="form-group row mt-3 mb-0">
                                    <div class="col text-center">
                                        @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Забыли пароль?') }}
                                        </a>
                                        @endif
                                    </div>
                                </div> -->
                            </form>
                        </div>
                    </div>
                </div>

            </div>
            <div class="card mt-3">
                <div class="row p-3">
                    <div class="col text-center">
                        <p>У вас ещё нет аккаунта? <a href="/register">Зарегистрироваться</a></p>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col text-center">
                    <p>Скоро на мобильных устройствах</p>
                </div>
            </div>
            <diw class="row">
                <div class="col text-center" >
                    <img src="{{ asset('src/img/apple.png') }}" style="height: 40px;" alt="">
                </div>
                <div class="col text-center">
                    <img src="{{ asset('src/img/android.png') }}" style="height: 40px;" alt="">
                </div>
            </diw>
        </div>
    </div>
</div>
@endsection