@extends('layouts.app')

@section('h')
Магазин
@endsection

@section('content')
<div class="form-group">
    <label for="club">Выберите клуб</label>
    <select class="form-control" id="club">
        <option>ФК "Сочи"</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
    </select>
</div>
<div class="input-group mb-4">
    <div class="input-group-prepend">
        <div class="login-icon search"><i class="fas fa-search"></i></div>
    </div>
    <input type="password" class="form-control login-input" name="password" id="password" required placeholder="Поиск">
    <div class="input-group-prepend">
        <div class="login-icon search"><i class="fas fa-filter"></i></div>
    </div>
</div>
<div class="row ">
    <div class="col">
        <div class="row">
            <div class="col">
                <div class="merch-item">
                    <img class="img-fluid" src="{{ asset('src/img/merch-2.jpg') }}" alt="">
                </div>
            </div>
            <div class="col">
                <p> <strong>Шарф SOCHI</strong> </p>
                <p>800 ₽</p>
                <button class="btn btn-outline-warning">Купить</button>
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col">
        <div class="row">
            <div class="col">
                <div class="merch-item">
                    <img class="img-fluid" src="{{ asset('src/img/merch-4.jpg') }}" alt="">
                </div>
            </div>
            <div class="col">
                <p> <strong>Шейкер</strong> </p>
                <p>350 ₽</p>
                <button class="btn btn-outline-warning">Купить</button>
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col">
        <div class="row">
            <div class="col">
                <div class="merch-item">
                    <img class="img-fluid" src="{{ asset('src/img/merch-6.jpg') }}" alt="">
                </div>
            </div>
            <div class="col">
                <p> <strong>Магнит</strong> </p>
                <p>250 ₽</p>
                <button class="btn btn-outline-warning">Купить</button>
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col">
        <div class="row">
            <div class="col">
                <div class="merch-item">
                    <img class="img-fluid" src="{{ asset('src/img/merch-5.jpg') }}" alt="">
                </div>
            </div>
            <div class="col">
                <p> <strong>Кружка</strong> </p>
                <p>300 ₽</p>
                <button class="btn btn-outline-warning">Купить</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')

@endsection