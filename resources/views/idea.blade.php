@extends('layouts.app')

@section('h')
Добавить идею
@endsection

@section('head')
<link href="src/libs/summernote/summernote.min.css" rel="stylesheet">
@endsection

@section('content')
<form action="/idea/create" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row post shadow-sm p-3 mt-3">
        <div class="col">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label for="nameIdea" class=" col-form-label">Название</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="nameIdea" name="title">
                        @error('title')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <!-- <div class="col-12">
                    <div class="form-group">
                        <label for="tags" class="col-form-label">Тэг</label>
                        <select class="form-control" id="tags" name="tags">
                            <option>Разработчики</option>
                            <option>Менеджеры</option>
                            <option>Отдел продаж</option>
                        </select>
                    </div>
                </div> -->
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="imageIdea">Изображение</label>
                        <input type="file" class="form-control-file @error('image') is-invalid @enderror" id="imageIdea" name="image">
                        <span class="text-black-50">Максимальный размер файла 2 мб</span>
                        @error('image')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="summernote">Описание идеи</label>
                        <textarea id="summernote" class="@error('text') is-invalid @enderror" name="text" required></textarea>
                        @error('text')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col"></div>
            </div>
            <div class="row justify-content-between">
                <div class="col-4">
                    <button type="submit" class="btn btn-warning rounded">Отправить на модерацию</button>
                </div>
                <div class="col-4">
                    <div class="g-recaptcha @error('g-recaptcha-response') is-invalid @enderror" data-sitekey="{{ config('recaptcha.GOOGLE_RECAPTCHA_KEY') }}">
                    </div>
                    @error('g-recaptcha-response')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>
    </div>

</form>
@endsection

@section('footer')
<script src="src/libs/summernote/summernote.min.js"></script>
<script src="src/libs/summernote/lang/summernote-ru-RU.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>

<script>
    $(document).ready(function() {
        $('#summernote').summernote();
    });
</script>
@endsection