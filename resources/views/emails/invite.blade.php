<!doctype html>
<html lang="ru">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="https://kit.fontawesome.com/6db2a57fbe.js" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('src/libs/bootstrap/css/bootstrap.min.css') }}">

    <title>Приложение для болельщиков</title>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col">
                Вас пригласили присоединиться к сообществу болельщиков на <a href="https://fanconnect.ru">fanconnect.ru</a>
            </div>
        </div>
    </div>
</body>
    <script src="{{ asset('src/libs/jquery/jquery-3.5.1.slim.min.js')}}"></script>
    <script src="{{ asset('src/libs/popper/popper.min.js')}}"></script>
    <script src="{{ asset('src/libs/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('src/libs/fontawesome/e9fbff9247.js')}}"></script>
</html>