@extends('layouts.app')

@section('head')
<link href="{{ asset('src/libs/summernote/summernote.min.css')}}" rel="stylesheet">
@endsection

@section('content')
<div class="row post shadow-sm">
    <div class="col">
        <div class="row p-2">
            <div class="col">
                <span style="font-size: 12px; color: gray">Никитин Алексей разработчики 12 июня 2020 13:55</span>
                <h4>{{ $ideas->title }}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col p-0 mt-2">
                <img class="img-fluid" src="{{ Storage::url($ideas->image) }}" alt="">
            </div>
        </div>
        <div class="row p-2">
            <div class="col text-left">
                
                <p>{!! $ideas->text !!}</p>
            </div>
        </div>
        <div class="row p-2">
            <div class="col text-left">
                <span class="vote vote-up" onclick='like({{ $ideas->id }})'>
                    <svg width="28" height="28" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="chevron-up">
                        <polyline fill="none" stroke="#000" stroke-width="1.03" points="4 13 10 7 16 13"></polyline>
                    </svg>
                </span>
                +<span class='rating' id="rating-{{ $ideas->id }}">{{ $ideas->reaction_like_count }}</span>
                <span href="#" class="vote vote-down" onclick="dislike({{ $ideas->id }})">
                    <svg width="28" height="28" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="chevron-down">
                        <polyline fill="none" stroke="#000" stroke-width="1.03" points="16 7 10 13 4 7"></polyline>
                    </svg>
                </span>
            </div>
            <div class="col text-right">
                <svg width="22" height="22" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="commenting">
                    <polygon fill="none" stroke="#000" points="1.5,1.5 18.5,1.5 18.5,13.5 10.5,13.5 6.5,17.5 6.5,13.5 1.5,13.5"></polygon>
                    <circle cx="10" cy="8" r="1"></circle>
                    <circle cx="6" cy="8" r="1"></circle>
                    <circle cx="14" cy="8" r="1"></circle>
                </svg>
                0
            </div>
        </div>
    </div>
</div>

<div class="row post shadow-sm mt-2">
    <div class="col">
        <div class="row p-2">
            <div class="col text-left">
                <h5>Комментарии</h5>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <form method="post">
                        <label for="summernote">Комментарии</label>
                        <textarea id="summernote" name="description"></textarea>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col"></div>
        </div>
        <div class="row">
            <div class="col-4">
                <button type="button" class="btn btn-primary rounded-0">Отправить</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script src="{{ asset('src/libs/summernote/summernote.min.js') }}"></script>
<script src="{{ asset('src/libs/summernote/lang/summernote-ru-RU.min.js') }}"></script>

<script>
    $(document).ready(function() {
        $('#summernote').summernote();
    });
</script>
@endsection