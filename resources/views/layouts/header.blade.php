<!-- <nav class="navbar navbar-expand-lg navbar-light bg-white sticky-top shadow-sm topbar">
     <a class="navbar-brand" href="/"><img src="{{ asset('src/img/logo.png')}}" class="logo" alt=""></a> 
    <h4><a href="/" class="headp">@yield('h')</a></h4>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">Идеи <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/cabinet">Кабинет</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/moderator">Кабинет модератора</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Поиск" aria-label="Search">
            <button class="btn btn-outline-primary rounded-0 my-2 my-sm-0" type="submit">Поиск</button>
        </form>
    </div>
</nav> -->


<nav class="navbar navbar-expand-lg navbar-light bg-white sticky-top shadow-sm topbar-lg">
    <div class="container">
        <a class="navbar-brand" href="/"><img src="{{ asset('src/img/logo.png')}}" class="logo" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/idea">Лента <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/cabinet">Кабинет</a>
                </li>
                @if(App\User::find(Auth::id())->admin == 1)
                <li class="nav-item">
                    <a class="nav-link" href="/admin">Админка</a>
                </li>
                @endif
                <!-- <li class="nav-item">
                <a class="nav-link" href="/moderator">Кабинет модератора</a>
            </li> -->
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                            {{ __('Выход') }}
                        </a>
                </li>
            </ul>
            <!-- <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Поиск" aria-label="Search">
            <button class="btn btn-outline-primary rounded-0 my-2 my-sm-0" type="submit">Поиск</button>
        </form> -->
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->email}} <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('cabinet') }}">
                            {{ __('Профиль') }}
                        </a>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                            {{ __('Выход') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
        </div>
    </div>
</nav>