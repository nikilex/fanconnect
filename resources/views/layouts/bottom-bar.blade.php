<nav class="navbar fixed-bottom navbar-light bg-light tabbar justify-content-around">
    <a href="/"><i class="fas fa-bullhorn icon-bottom"></i></a>
    <a href="{{ route('ticket') }}"><i class="fas fa-ticket-alt icon-bottom"></i></a> 
    <a href="/create"><i class="fas fa-plus icon-bottom"></i></a>
    <a href="{{ route('store') }}"> <i class="fas fa-store icon-bottom"></i></a>
    <a href="{{ route('cabinet') }}"><i class="fas fa-user icon-bottom"></i></a>
</nav>