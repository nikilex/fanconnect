<!-- Site footer -->
<footer class="site-footer mt-5">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-sm-12 col-md-6">
                <h6>Разработка команды Форвард</h6>
                <a href="https://forward-web.turbo.site/"><img class="mt-3" src="{{ asset('src/img/logo-forward.webp') }}" style="width: 200px" alt=""></a> 
            </div>

            <div class="col-xs-6 col-md-3">
                <h6>Ссылки</h6>
                <ul class="footer-links">
                    <li><a href="/">Лента</a></li>
                    <li><a href="/cabinet">Кабинет</a></li>
                    <li><a href="https://forward-web.turbo.site/">Связаться с нами</a></li>
                </ul>
            </div>
        </div>
        <hr>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <p class="copyright-text">Copyright &copy; 2019 Все права защищены
                    <a href="https://forward-web.turbo.site/">Форвард</a>.
                </p>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <ul class="social-icons">
                    <li><a class="facebook" href="https://www.facebook.com/%D0%A4%D0%BE%D1%80%D0%B2%D0%B0%D1%80%D0%B4-107652107741754/?modal=admin_todo_tour"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="instagram" href="https://www.instagram.com/forvardweb/"><i class="fa fa-instagram"></i></a></li>
                    <li><a class="vk" href="https://vk.com/forward_it"><i class="fa fa-vk"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>