<!doctype html>
<html lang="ru">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="https://kit.fontawesome.com/6db2a57fbe.js" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('src/libs/owl/css/owl.carousel.css') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('src/libs/bootstrap/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset('src/css/style.css') }}">

    @yield('head')

    <title>Приложение для болельщиков</title>
</head>

<body>

    @guest
    @else
        @include('layouts.header')
    @endguest
    <div class="container">
        @yield('content')
        <!-- <div class="row justify-content-between">
            <div class="col main-bar">
                
            </div>
        </div> -->
    </div>

    @include('layouts.footer')
   


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('src/libs/jquery/jquery-3.5.1.slim.min.js')}}"></script>
    <script src="{{ asset('src/libs/popper/popper.min.js')}}"></script>
    <script src="{{ asset('src/libs/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('src/libs/fontawesome/e9fbff9247.js')}}"></script>
    <script src="{{ asset('src/libs/owl/js/owl.carousel.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js" integrity="sha256-T/f7Sju1ZfNNfBh7skWn0idlCBcI3RwdLSS4/I7NQKQ=" crossorigin="anonymous"></script>

    <script>
        function rating(id, type) {
            var vote = $('#rating-' + id);
            var number = parseInt(vote.html());
            var plus = number + 1;
            var minus = number - 1;

            //vote.html(number++)
            if (type == 'up') {
                vote.html(plus)
            } else {
                vote.html(minus)
            }

            axios
                .post('/idea/like', {
                    id: id
                })
                .then(function() {
                    console.log('Запрос прошел успешно');
                })
        }
        function like(id){
            var vote = $('#rating-' + id);
            var number = parseInt(vote.html());
            var plus = number + 1;

            axios
                .post('/idea/like', {
                    id: id
                })
                .then(function(res) {
                    vote.html(plus);
                    console.log(res);
                })
        }
        function dislike(id){
            var vote = $('#rating-' + id);
            var number = parseInt(vote.html());
            var minus = number - 1;

            axios
                .post('/idea/dislike', {
                    id: id
                })
                .then(function(res) {
                    vote.html(minus);
                    console.log(res);
                })
        }
    </script>
    @yield('footer')
</body>

</html>