@extends('layouts.app')

@section('h')
Админка
@endsection

@section('head')

@endsection

@section('content')

<div class="row post shadow-sm p-3 mt-3">
    <div class="col-3">
        @include('admin.layouts.menu')
    </div>
    <div class="col">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название</th>
                    <th scope="col">Описание</th>
                    <th scope="col">Публикация</th>
                    <th scope="col">Действие</th>
                </tr>
            </thead>
            <tbody>
                @foreach($ideas as $idea)
                <tr>
                    <th scope="row">{{ $idea->id }}</th>
                    <td>{{ $idea->title }}</td>
                    <td>{{ $idea->description }}</td>
                    <td>{{ ($idea->published == 0) ? 'Не опубликована' : 'Опубликована' }}</td>
                    <td>
                        <form action="/idea/published" method="post">
                            @csrf
                            <input type="hidden" name="idea_id" value="{{$idea->id}}">
                            <input type="submit" class="btn btn-outline-primary" value="{{ ($idea->published == 0) ? 'Опубликовать' : 'Снять с публикации'}}">
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $ideas->links() }}
    </div>
</div>

@endsection

@section('footer')

@endsection