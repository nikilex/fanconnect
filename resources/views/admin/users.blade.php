@extends('layouts.app')

@section('h')
Админка
@endsection

@section('head')

@endsection

@section('content')

<div class="row post shadow-sm p-3 mt-3">
    <div class="col-3">
        @include('admin.layouts.menu')
    </div>
    <div class="col">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Никнейм</th>
                    <th scope="col">ФИО</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Телефон</th>
                    <th scope="col">Клуб</th>
                    <th scope="col">Права</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr>
                    <th scope="row">{{ $user->id }}</th>
                    <td>{{ $user->email }}</td>
                    <td>{{ (isset($user->profile)) ? $user->profile->name : '-' }}</td>
                    <td>{{ (isset($user->profile)) ? $user->profile->email : '-' }}</td>
                    <td>{{ (isset($user->profile)) ? $user->profile->phone : '-' }}</td>
                    <td>-</td>
                    <td>{{ ($user->admin == 1) ? 'Админ' : 'Пользователь' }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $users->links() }}
    </div>
</div>

@endsection

@section('footer')

@endsection