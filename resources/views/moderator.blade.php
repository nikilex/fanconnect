@extends('layouts.app')

@section('content')
<div class="row post shadow-sm p-3">
    <div class="col">
        <div class="row">
            <div class="col-3">
                <img src="src/img/mod.jpg" class=" avatar" alt="">
            </div>
            <div class="col align-self-end">
                <h5>Вячеслав Еремин</h5>
                <span>Специалист по работе с болельщиками</span>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col">
                <p>Проверил идей <span style="color:blue;">784,</span> одобрил <span style="color:green;">236</span></p>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col">
                <h5>6790</h5>
                <span>рейтинг</span>
            </div>
            <div class="col">
                <h5>589</h5>
                <span>комментариев</span>
            </div>
            <div class="col">
                <h5>784</h5>
                <span>идей</span>
            </div>
            <div class="col">
                <h5>236</h5>
                <span>в топе</span>
            </div>
        </div>
    </div>
</div>

<div class="row post shadow-sm mt-5 p-2 mb-5">
    <div class="col">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" id="users-tab" data-toggle="tab" href="#users" role="tab" aria-controls="users" aria-selected="true">Зарегистрированные фанаты</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Свежие идеи</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Топовые идеи на модерацию</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="matrix-tab" data-toggle="tab" href="#matrix" role="tab" aria-controls="matrix" aria-selected="false">Матрица рассадки</a>
            </li>

        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="users" role="tabpanel" aria-labelledby="users-tab">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Имя</th>
                            <th scope="col">Фамилия</th>
                            <th scope="col">Статус</th>
                            <th scope="col">Приглашения</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Никтин</td>
                            <td>Алексей</td>
                            <td style="color: red;">На модерации</td>
                            <td><a class="btn btn-link" data-toggle="collapse" href="#alex" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    Посмотреть
                                </a>
                            </td>
                        </tr>
                        <tr class="collapse" id="alex">
                            <th scope="row"></th>
                            <td>-</td>
                            <td>-</td>
                            <td style="color: blue">-</td>
                        </tr>
                        
                        <tr>
                            <th scope="row">2</th>
                            <td>Богданов</td>
                            <td>Егор</td>
                            <td style="color: green">Ермак</td>
                            <td><a class="btn btn-link" data-toggle="collapse" href="#egor" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    Посмотреть
                                </a>
                            </td>
                        </tr>
                        <tr class="collapse" id="egor">
                            <th scope="row"></th>
                            <td>Дуров</td>
                            <td>Павел</td>
                            <td style="color: blue">Пионер</td>
                        </tr>
                        <tr class="collapse" id="egor">
                            <th scope="row"></th>
                            <td>Роженцев</td>
                            <td>Сергей</td>
                            <td style="color: green">Ермак</td>
                        </tr>
                        <tr class="collapse" id="egor">
                            <th scope="row"></th>
                            <td>Саймойлов</td>
                            <td>Елисей</td>
                            <td style="color: blue">Пионер</td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Обухов</td>
                            <td>Иван</td>
                            <td style="color: blue">Пионер</td>
                            <td><a class="btn btn-link" data-toggle="collapse" href="#ivan" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    Посмотреть
                                </a>
                            </td>
                        </tr>
                        <tr class="collapse" id="ivan">
                            <th scope="row"></th>
                            <td>Демин</td>
                            <td>Дмитрий</td>
                            <td style="color: blue">Пионер</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade show " id="home" role="tabpanel" aria-labelledby="home-tab">
                <ul class="list-group list-group-flush p-3">
                    <li class="list-group-item">Новая идея 1 | рейтинг: <span style="color:red;">+15</span></li>
                    <li class="list-group-item">Новая идея 2 | рейтинг: <span style="color:green;">+51</span></li>
                    <li class="list-group-item">Новая идея 3 | рейтинг: <span style="color:green;">+75</span></li>
                    <li class="list-group-item">Новая идея 4 | рейтинг: <span style="color:red;">+49</span></li>
                    <li class="list-group-item">Новая идея 5 | рейтинг: <span style="color:green;">+156</span></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <ul class="list-group list-group-flush p-3">
                    <li class="list-group-item">Топовая идея 1 | рейтинг: <span style="color:green;">+1578</span></li>
                    <li class="list-group-item">Топовая идея 2 | рейтинг: <span style="color:green;">+4515</span></li>
                    <li class="list-group-item">Топовая идея 3 | рейтинг: <span style="color:green;">+1495</span></li>
                    <li class="list-group-item">Топовая идея 4 | рейтинг: <span style="color:green;">+7217</span></li>
                    <li class="list-group-item">Топовая идея 5 | рейтинг: <span style="color:yellow;">+10563</span></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="matrix" role="tabpanel" aria-labelledby="matrix-tab">
                <img class="img-fluid" src="{{ asset('src/img/page-0001.jpg') }}" alt="">
            </div>
        </div>
    </div>
</div>

<div class="row post shadow-sm mt-5 p-2 mb-5">
    <div class="col">
        <h4 style="text-align: center;" class="mb-3 mt-3">Бригады</h4>
        <img class="img-fluid" src="{{ asset('src/img/brig.jpg') }}" alt="">
    </div>
</div>
@endsection