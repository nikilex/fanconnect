@extends('layouts.app')

@section('h')
Профиль
@endsection



@section('content')
<div class="alert alert-warning text-center" role="alert">
    Сайт находится на стадии разработки, в данный момент доступна только регистрация
</div>
@if (session('success'))
    <div class="alert alert-success mt-3">
        {{ session('success') }}
    </div>
    @endif

<div class="row post shadow-sm p-3 mt-3">
    <div class="col">
        <div class="row">
            <div class="col col-xl-3">
                <img src="{{ asset('src/img/no-photo-man.jpg')}}" class=" avatar" alt="">
            </div>
            <div class="col align-self-end">
                <h5>{{ Auth::user()->email }}</h5>
                <span>Болельщик</span>
            </div>
        </div>
        <!-- <div class="row mt-5">
            <div class="col">
                <p>Подал идей <span style="color:blue;">0,</span> одобрили <span style="color:green;">0</span></p>
            </div>
        </div> -->
        <div class="row mt-5">
            <div class="col">
                <h5>{{ (isset($user->profile)) ? ($user->profile->rating == NULL) ? '0' : $user->profile->rating : '0' }}</h5>
                <span>рейтинг</span>
            </div>
            <div class="col">
                <h5>0</h5>
                <span>комментариев</span>
            </div>
            <div class="col">
                <h5>0</h5>
                <span>идей в топе</span>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-warning btn-block" data-toggle="modal" data-target="#invite">
                    Пригласить друга
                </button>
                <a href="/create" class="btn btn-outline-primary btn-block">Добавить идею</a>
            </div>
        </div>
    </div>
</div>
<div class="row post shadow-sm mt-3 p-2">
    <div class="col">
        <div class="row">
            <div class="col">
                <h5>Сервисы</h5>
            </div>
        </div>
        <div class="row">
            <div class="col p-0">
                <div class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="news">
                            <a href="https://taxi.yandex.ru/"><img src="{{ asset('src/img/taxi.png') }}" style="width: 100px" alt=""></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="news">
                            <a href="https://eda.yandex/"><img src="{{ asset('src/img/eda.png') }}" style="width: 100px" alt=""></a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="news">
                            <a href="https://www.delivery-club.ru/"> <img src="{{ asset('src/img/dc.png') }}" style="width: 100px" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row post shadow-sm mt-3 p-2">
    <div class="col">
        <div class="row">
            <div class="col">
                <h5>Профиль</h5>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <form method="post" action="{{ route('profile.update') }}">
                    @csrf
                    <div class="form-group">
                        <label for="name">ФИО</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ (isset($user->profile)) ? $user->profile->name : ''}}" placeholder="ФИО">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Электронный адрес</label>
                        <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ (isset($user->profile)) ? $user->profile->email : '' }}" placeholder="name@example.com">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="phone">Телефон</label>
                        <input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" value="{{ (isset($user->profile)) ? $user->profile->phone : '' }}" placeholder="+7(999)777-66-55">
                        @error('phone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="club">Мой любимый клуб</label>
                        <input type="text" class="form-control @error('club_name') is-invalid @enderror" id="club_name" name="club_name" value="{{ ( $club != '') ? $club->name : ''}}" placeholder="Название клуба">
                        @error('club_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                       
                        @error('club_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <button class="btn btn-outline-success">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<div class="row post shadow-sm mt-3 p-2">
    <div class="col">
        <div class="row">
            <div class="col">
                <h5>Награды</h5>
            </div>
        </div>

        <div class="row justify-content-between">
            <div class="col">
                <img src="{{ asset('src/img/fun-1.png') }}" class="bonus img-fluid" alt="">
            </div>
            <div class="col">
                <img src="{{ asset('src/img/fun-2.png') }}" class="bonus img-fluid" alt="">
            </div>
            <div class="col">
                <img src="{{ asset('src/img/fun-3.png') }}" class="bonus img-fluid" alt="">
            </div>
        </div>
    </div>
</div>


<!-- <div class="row right-first rigth-bar-item p-3 d-md-none d-block">
    <div class="col">
    <h5>Моё дерево</h5>
                <img src="{{ asset('src/img/tree.jpg') }}" class="img-fluid" alt="">
    </div>
</div> -->
<p class="text-center mt-5">Мои идеи</p>
@foreach ($ideas as $key => $idea)
@if($idea->published != 0)
<div class="row post shadow-sm mt-5">

    <div class="col">
        <div class="row p-2">
            <div class="col text-left">
                <!-- <span style="font-size: 12px; color: gray">Никитин Алексей разработчики 12 июня 2020 13:55</span> -->
                <a href="/single/{{ $idea->id }}" class="title">
                    <h4> {{ $idea->title }}</h4>
                </a>
                <p>{!! $idea->text !!}</p>
            </div>
        </div>
        <div class="row p-2">
            <!-- <div class="col text-left">
                <span class="vote" onclick="like({{ $idea->id }})">
                    <svg width="28" height="28" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="chevron-up">
                        <polyline fill="none" stroke="#000" stroke-width="1.03" points="4 13 10 7 16 13"></polyline>
                    </svg>
                </span>
                +<span class='rating' id="rating-{{ $idea->id }}">{{ $idea->reaction_like_count }}</span>
                <span class="vote" onclick="dislike({{ $idea->id }})">
                    <svg width="28" height="28" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="chevron-down">
                        <polyline fill="none" stroke="#000" stroke-width="1.03" points="16 7 10 13 4 7"></polyline>
                    </svg>
                </span>
            </div>
            <div class="col text-right">
                <svg width="22" height="22" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="commenting">
                    <polygon fill="none" stroke="#000" points="1.5,1.5 18.5,1.5 18.5,13.5 10.5,13.5 6.5,17.5 6.5,13.5 1.5,13.5"></polygon>
                    <circle cx="10" cy="8" r="1"></circle>
                    <circle cx="6" cy="8" r="1"></circle>
                    <circle cx="14" cy="8" r="1"></circle>
                </svg>
                0
            </div> -->
        </div>
    </div>
</div>
@endif
@endforeach



<!-- Modal -->
<div class="modal fade" id="invite" tabindex="-1" role="dialog" aria-labelledby="invileLable" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="invileLable">Пригласить друга</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('cabinet.invite') }}" method="post">
                @csrf
                <div class="modal-body">

                    <div class="form-group">
                        <label for="emailFriend">Электронный адрес друга</label>
                        <input type="email" class="form-control" id="emailFriend" name="emailFriend" value="alex_mobi@mail.ru" placeholder="name@example.com">
                    </div>
                    <div class="g-recaptcha @error('g-recaptcha-response') is-invalid @enderror" data-sitekey="{{ config('recaptcha.GOOGLE_RECAPTCHA_KEY') }}">
                    </div>
                    @error('g-recaptcha-response')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    <button type="submit" class="btn btn-primary">Отправить приглашение</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        autoplay: false,
        // stagePadding: 50,
        responsive: {
            0: {
                items: 3
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    })
</script>
@endsection