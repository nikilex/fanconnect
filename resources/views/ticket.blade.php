@extends('layouts.app')

@section('h')
Билеты
@endsection

@section('content')
<div class="form-group">
    <label for="club">Выберите стадион</label>
    <select class="form-control" id="club">
        <option>Стадион Фишт</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
    </select>
</div>
<div class="input-group mb-4">
    <div class="input-group-prepend">
        <div class="login-icon search"><i class="fas fa-search"></i></div>
    </div>
    <input type="password" class="form-control login-input" name="password" id="password" required placeholder="Поиск">
    <div class="input-group-prepend">
        <div class="login-icon search"><i class="fas fa-filter"></i></div>
    </div>
</div>
<div class="row shadow-sm">
    <div class="col">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th>Мероприятие</th>
                    <th>Дата проведения</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>СОЧИ - УРАЛ</td>
                    <td>30 августа 2020, 20:00 (вс)</td>
                    <td> <a href="">Купить</a> </td>
                </tr>
                <tr>
                    <td>СОЧИ - ЗЕНИТ</td>
                    <td>20 сентября 2020, 20:00 (вс)</td>
                    <td> <a href="">Купить</a> </td>
                </tr>
                <tr>
                    <td>СОЧИ - РОСТОВ</td>
                    <td>30 сентября 2020, 20:00 (вс)</td>
                    <td> <a href="">Купить</a> </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('footer')

@endsection