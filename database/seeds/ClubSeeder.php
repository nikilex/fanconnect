<?php

use Illuminate\Database\Seeder;
use App\Models\Club;

class ClubSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clubs = [
            'Манчестер Сити',
            'Реал Мадрид',
            'Барселона',
            'Бавария Мюнхен',
            'Пари Сен-Жермен',
            'Ливерпуль',
            'Тоттенхэм',
            'Ювентус',
            'Барселона',
            'Челси',
            'Манчестер Юнайтед'];

        foreach($clubs as $club){
            Club::create([
                'name' => $club,
            ]);
        }
    }
}
