-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Хост: 10.19.5.19:3307
-- Время создания: Сен 06 2020 г., 00:56
-- Версия сервера: 10.5.4-MariaDB-1:10.5.4+maria~buster-log
-- Версия PHP: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `c24870_fanclub_na4u_ru`
--

-- --------------------------------------------------------

--
-- Структура таблицы `love_reactants`
--

CREATE TABLE `love_reactants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `love_reactants`
--

INSERT INTO `love_reactants` (`id`, `type`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\Idea', '2020-06-20 12:43:17', '2020-06-20 12:43:17'),
(2, 'App\\Models\\Idea', '2020-06-20 12:56:26', '2020-06-20 12:56:26'),
(3, 'App\\Models\\Idea', '2020-06-20 14:14:16', '2020-06-20 14:14:16'),
(4, 'App\\Models\\Idea', '2020-06-20 14:14:46', '2020-06-20 14:14:46'),
(5, 'App\\Models\\Idea', '2020-06-20 14:26:09', '2020-06-20 14:26:09'),
(6, 'App\\Models\\Idea', '2020-06-20 14:27:56', '2020-06-20 14:27:56'),
(7, 'App\\Models\\Idea', '2020-06-20 14:38:25', '2020-06-20 14:38:25'),
(8, 'App\\Models\\Idea', '2020-06-21 11:25:38', '2020-06-21 11:25:38'),
(9, 'App\\Models\\Idea', '2020-08-21 11:19:08', '2020-08-21 11:19:08');

-- --------------------------------------------------------

--
-- Структура таблицы `love_reactant_reaction_counters`
--

CREATE TABLE `love_reactant_reaction_counters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reactant_id` bigint(20) UNSIGNED NOT NULL,
  `reaction_type_id` bigint(20) UNSIGNED NOT NULL,
  `count` bigint(20) UNSIGNED NOT NULL,
  `weight` decimal(13,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `love_reactant_reaction_counters`
--

INSERT INTO `love_reactant_reaction_counters` (`id`, `reactant_id`, `reaction_type_id`, `count`, `weight`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1.00, '2020-06-20 12:49:55', '2020-06-20 12:49:55'),
(2, 2, 1, 3, 3.00, '2020-06-20 13:03:03', '2020-08-21 11:26:10'),
(3, 2, 2, 1, -1.00, '2020-06-20 13:07:37', '2020-06-20 13:07:37'),
(4, 8, 1, 1, 1.00, '2020-08-21 07:07:27', '2020-08-21 07:07:27'),
(5, 8, 2, 1, -1.00, '2020-08-21 07:07:32', '2020-08-21 07:07:32'),
(6, 9, 1, 1, 1.00, '2020-08-21 11:26:06', '2020-08-21 11:26:06');

-- --------------------------------------------------------

--
-- Структура таблицы `love_reactant_reaction_totals`
--

CREATE TABLE `love_reactant_reaction_totals` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reactant_id` bigint(20) UNSIGNED NOT NULL,
  `count` bigint(20) UNSIGNED NOT NULL,
  `weight` decimal(13,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `love_reactant_reaction_totals`
--

INSERT INTO `love_reactant_reaction_totals` (`id`, `reactant_id`, `count`, `weight`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1.00, '2020-06-20 12:49:55', '2020-06-20 12:49:55'),
(2, 2, 4, 2.00, '2020-06-20 13:03:03', '2020-08-21 11:26:10'),
(3, 8, 2, 0.00, '2020-08-21 07:07:27', '2020-08-21 07:07:32'),
(4, 9, 1, 1.00, '2020-08-21 11:26:06', '2020-08-21 11:26:06');

-- --------------------------------------------------------

--
-- Структура таблицы `love_reacters`
--

CREATE TABLE `love_reacters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `love_reacters`
--

INSERT INTO `love_reacters` (`id`, `type`, `created_at`, `updated_at`) VALUES
(1, 'App\\User', '2020-06-20 12:34:57', '2020-06-20 12:34:57'),
(2, 'App\\User', '2020-06-20 13:08:47', '2020-06-20 13:08:47'),
(3, 'App\\User', '2020-08-21 11:24:57', '2020-08-21 11:24:57'),
(4, 'App\\User', '2020-09-05 13:30:04', '2020-09-05 13:30:04');

-- --------------------------------------------------------

--
-- Структура таблицы `love_reactions`
--

CREATE TABLE `love_reactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reactant_id` bigint(20) UNSIGNED NOT NULL,
  `reacter_id` bigint(20) UNSIGNED NOT NULL,
  `reaction_type_id` bigint(20) UNSIGNED NOT NULL,
  `rate` decimal(4,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `love_reactions`
--

INSERT INTO `love_reactions` (`id`, `reactant_id`, `reacter_id`, `reaction_type_id`, `rate`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1.00, '2020-06-20 12:49:55', '2020-06-20 12:49:55'),
(2, 2, 1, 1, 1.00, '2020-06-20 13:03:03', '2020-06-20 13:03:03'),
(3, 2, 1, 2, 1.00, '2020-06-20 13:07:36', '2020-06-20 13:07:36'),
(4, 2, 2, 1, 1.00, '2020-06-20 13:09:04', '2020-06-20 13:09:04'),
(5, 8, 1, 1, 1.00, '2020-08-21 07:07:26', '2020-08-21 07:07:26'),
(6, 8, 1, 2, 1.00, '2020-08-21 07:07:32', '2020-08-21 07:07:32'),
(7, 9, 3, 1, 1.00, '2020-08-21 11:26:06', '2020-08-21 11:26:06'),
(8, 2, 3, 1, 1.00, '2020-08-21 11:26:10', '2020-08-21 11:26:10');

-- --------------------------------------------------------

--
-- Структура таблицы `love_reaction_types`
--

CREATE TABLE `love_reaction_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mass` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `love_reaction_types`
--

INSERT INTO `love_reaction_types` (`id`, `name`, `mass`, `created_at`, `updated_at`) VALUES
(1, 'Like', 1, '2020-06-20 12:34:20', '2020-06-20 12:34:20'),
(2, 'Dislike', -1, '2020-06-20 12:34:20', '2020-06-20 12:34:20');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `love_reactants`
--
ALTER TABLE `love_reactants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `love_reactants_type_index` (`type`);

--
-- Индексы таблицы `love_reactant_reaction_counters`
--
ALTER TABLE `love_reactant_reaction_counters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `love_reactant_reaction_counters_reactant_reaction_type_index` (`reactant_id`,`reaction_type_id`),
  ADD KEY `love_reactant_reaction_counters_reaction_type_id_foreign` (`reaction_type_id`);

--
-- Индексы таблицы `love_reactant_reaction_totals`
--
ALTER TABLE `love_reactant_reaction_totals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `love_reactant_reaction_totals_reactant_id_foreign` (`reactant_id`);

--
-- Индексы таблицы `love_reacters`
--
ALTER TABLE `love_reacters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `love_reacters_type_index` (`type`);

--
-- Индексы таблицы `love_reactions`
--
ALTER TABLE `love_reactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `love_reactions_reactant_id_reaction_type_id_index` (`reactant_id`,`reaction_type_id`),
  ADD KEY `love_reactions_reactant_id_reacter_id_reaction_type_id_index` (`reactant_id`,`reacter_id`,`reaction_type_id`),
  ADD KEY `love_reactions_reactant_id_reacter_id_index` (`reactant_id`,`reacter_id`),
  ADD KEY `love_reactions_reacter_id_reaction_type_id_index` (`reacter_id`,`reaction_type_id`),
  ADD KEY `love_reactions_reaction_type_id_foreign` (`reaction_type_id`);

--
-- Индексы таблицы `love_reaction_types`
--
ALTER TABLE `love_reaction_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `love_reaction_types_name_index` (`name`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `love_reactants`
--
ALTER TABLE `love_reactants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `love_reactant_reaction_counters`
--
ALTER TABLE `love_reactant_reaction_counters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `love_reactant_reaction_totals`
--
ALTER TABLE `love_reactant_reaction_totals`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `love_reacters`
--
ALTER TABLE `love_reacters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `love_reactions`
--
ALTER TABLE `love_reactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `love_reaction_types`
--
ALTER TABLE `love_reaction_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `love_reactant_reaction_counters`
--
ALTER TABLE `love_reactant_reaction_counters`
  ADD CONSTRAINT `love_reactant_reaction_counters_reactant_id_foreign` FOREIGN KEY (`reactant_id`) REFERENCES `love_reactants` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `love_reactant_reaction_counters_reaction_type_id_foreign` FOREIGN KEY (`reaction_type_id`) REFERENCES `love_reaction_types` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `love_reactant_reaction_totals`
--
ALTER TABLE `love_reactant_reaction_totals`
  ADD CONSTRAINT `love_reactant_reaction_totals_reactant_id_foreign` FOREIGN KEY (`reactant_id`) REFERENCES `love_reactants` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `love_reactions`
--
ALTER TABLE `love_reactions`
  ADD CONSTRAINT `love_reactions_reactant_id_foreign` FOREIGN KEY (`reactant_id`) REFERENCES `love_reactants` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `love_reactions_reacter_id_foreign` FOREIGN KEY (`reacter_id`) REFERENCES `love_reacters` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `love_reactions_reaction_type_id_foreign` FOREIGN KEY (`reaction_type_id`) REFERENCES `love_reaction_types` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
